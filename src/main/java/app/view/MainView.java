package app.view;

import app.Constant;
import app.model.service.FileInfoHandler;
import app.model.service.JavaFileProcessor;
import app.model.service.Serializer;
import app.model.domain.Student;

import app.model.service.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainView {

  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner scn = new Scanner(System.in);
  private static final Logger logger = LogManager.getLogger(MainView.class);

  public MainView() {
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - Serialization test");
    menu.put("2", "  2 - BufferedReader test");
    menu.put("3", "  3 - Java code parser test");
    menu.put("4", "  4 - File explorer test");
    menu.put("q", "  q - exit");
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);
  }

  private void pressButton1() throws IOException {
    Student student = new Student();
    logger.trace("Enter student name: ");
    student.name = scn.nextLine();
    logger.trace("Enter student age: ");
    student.age = scn.nextInt();
    logger.trace("Enter study group: ");
    student.group = scn.next();
    logger.info(student.toString() + " created");
    logger.trace("Serializing student object...");
    Serializer serializer = new Serializer();
    serializer.serializeObject(student, new File(Constant.OBJECT_SERIALIZATION_FILE));
    logger.info("student object saved in file: " + Constant.OBJECT_SERIALIZATION_FILE);
    logger.trace("Deserializing student object...");
    Student deserializedStudent = (Student) serializer.deserializeObject(new File(Constant.OBJECT_SERIALIZATION_FILE));
    logger.info("student obj after deserialization: " + deserializedStudent.toString());
  }

  private void pressButton2() {
    BufferedReader bufferedReader = new BufferedReader(new File(Constant.TEST_TXT_FILE));
    logger.trace("Reading 100 mb file with buffer");
    long currentTime = System.currentTimeMillis();
    bufferedReader.read(5);
    logger.info("5 byte buffer reading runtime: "
        + (System.currentTimeMillis() - currentTime) + " milliseconds");
    currentTime = System.currentTimeMillis();
    bufferedReader.read(50);
    logger.info("50 byte buffer reading runtime: "
        + (System.currentTimeMillis() - currentTime) + " milliseconds");
    currentTime = System.currentTimeMillis();
    bufferedReader.read(500);
    logger.info("500 byte buffer reading runtime: "
        + (System.currentTimeMillis() - currentTime) + " milliseconds");
    currentTime = System.currentTimeMillis();
    bufferedReader.read(1024);
    logger.info("1 kb buffer reading runtime: "
        + (System.currentTimeMillis() - currentTime) + " milliseconds");
    currentTime = System.currentTimeMillis();
    bufferedReader.read(2024);
    logger.info("2 kb buffer reading runtime: "
        + (System.currentTimeMillis() - currentTime) + " milliseconds");
    currentTime = System.currentTimeMillis();
    bufferedReader.read(15000);
    logger.info("15 kb buffer reading runtime: "
        + (System.currentTimeMillis() - currentTime) + " milliseconds");
    currentTime = System.currentTimeMillis();
    bufferedReader.read(20000);
    logger.info("200 kb buffer reading runtime: "
        + (System.currentTimeMillis() - currentTime) + " milliseconds");
  }

  private void pressButton3() {
    JavaFileProcessor javaFileProcessor = new JavaFileProcessor(new File(Constant.TEST_JAVA_FILE));
    logger.trace("Getting all comments from java src file: " + Constant.TEST_JAVA_FILE);
    logger.info("Here is all comments from file: " + javaFileProcessor.getComments());
  }

  private void pressButton4() {
    FileInfoHandler fileInfoHandler = new FileInfoHandler();
    logger.info("Getting project directory content...");
    fileInfoHandler.displayDirectoryContents(new File(Constant.PROJECT_PATH));
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.print("Please, select menu option: ");
      keyMenu = scn.nextLine();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("q"));
  }
}
