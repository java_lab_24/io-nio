package app.model.domain;

import java.io.Serializable;

public class Student implements Serializable {

  private static final long serialVersionUID = 86834525811L;

  public static transient String university;
  public String name;
  public int age;
  public String group;

  public Student() {
    university = "NULP";
  }

  @Override
  public String toString() {
    return "Student{" +
        "name='" + name + '\'' +
        ", age=" + age +
        ", group='" + group + '\'' +
        '}';
  }
}
