package app.model.service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BufferedReader {

  private final File file;
  private int data;
  private static final Logger logger = LogManager.getLogger(BufferedReader.class);

  public BufferedReader(File file) {
    this.file = file;
  }
  
  public void read(int bufferSize) {
    try (java.io.BufferedReader bufferedReader =
        new java.io.BufferedReader(new FileReader(file), bufferSize)) {
      data = bufferedReader.read();
      while (data != -1) {
        data = bufferedReader.read();
      }
    } catch (IOException e) {
      e.printStackTrace();
      logger.error(e);
    }
  }
}
