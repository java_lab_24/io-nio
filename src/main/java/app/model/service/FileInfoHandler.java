package app.model.service;

import java.io.File;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileInfoHandler {

  private static final Logger logger = LogManager.getLogger(FileInfoHandler.class);

  public void displayDirectoryContents(File dir) {
    File[] files = dir.listFiles();
    for (File file : files) {
      if (file.isDirectory()) {
        System.out.println("dir:" + file.getName());
        displayDirectoryContents(file);
      } else {
        System.out.println("     file:" + file.getName());
      }
    }
  }

}
