package app.model.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JavaFileProcessor {

  private File javaFile;
  private int data;

  public JavaFileProcessor(File javaFile) {
    this.javaFile = javaFile;
  }

  public List<String> getComments() {
    List<String> comments = new LinkedList<>();
    StringBuilder comment = new StringBuilder();
    try (java.io.BufferedReader reader = new BufferedReader(new FileReader(javaFile))) {
      data = reader.read();
      while (data != -1) {
        if ((char) data == '/' && (data = reader.read()) == '/') {
          do {
            data = reader.read();
            comment.append((char) data);
          } while (data != '\n');
          comments.add(comment.toString());
          comment.delete(0, comment.length());
        }
        data = reader.read();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return comments;
  }
}
