package app.model.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Serializer {

  private static final Logger logger = LogManager.getLogger(Serializer.class);

  public void serializeObject(Object obj, File file) {
    try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(file))) {
      output.writeObject(obj);
    } catch (IOException e) {
      e.printStackTrace();
      logger.error(e);
    }
  }

  public Object deserializeObject(File file) {
    try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(file))) {
      return input.readObject();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
      logger.error(e);
    }
    return null;
  }
}
