package app;

public class Constant {

  public static final String OBJECT_SERIALIZATION_FILE = "D:\\Me\\epamUniversity\\offlinePart\\task08\\task08_IO_NIO\\src\\main\\resources\\studentSerialized.txt";
  public static final String TEST_TXT_FILE = "D:\\Me\\epamUniversity\\offlinePart\\task08\\task08_IO_NIO\\src\\main\\resources\\test.txt";
  public static final String TEST_JAVA_FILE = "D:\\Me\\epamUniversity\\offlinePart\\task08\\task08_IO_NIO\\src\\main\\resources\\TestClass.java";
  public static final String PROJECT_PATH = "D:\\Me\\epamUniversity\\offlinePart\\task08\\task08_IO_NIO";
}
